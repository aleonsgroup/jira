package leonov.logic;

import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.google.gson.Gson;
import webwork.action.ActionContext;

import java.util.ArrayList;
import java.util.Map;

public class AddUser extends JiraWebActionSupport {

    UserService userService;

    public AddUser() {
        this.userService = ComponentAccessor.getComponent(UserService.class);
    }

    @Override
    public String execute() throws Exception {
        Map param = ActionContext.getParameters();
        if(param.get("user_add_data")!=null) {
            String[] json = (String[]) param.get("user_add_data");
            ArrayList<Map<String,String>> usersMap = new Gson().fromJson(json[0], ArrayList.class);
            for (Map<String,String> userMap: usersMap){
                try {
                    userService.createUser(
                            userService.validateCreateUser(
                                    UserService.CreateUserRequest.withUserDetails(getLoggedInUser(),
                                            userMap.get("username"), userMap.get("password"), userMap.get("mail"), userMap.get("displayName"))
                            )
                    );
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }

        }
        return super.execute();

    }
}
